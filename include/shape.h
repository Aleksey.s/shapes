#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <windows.h>
#include <memory>
#include <vector>
#include <cmath>
#include <exception>

namespace shp {

class IShape
{
public:
    IShape() = default;
    virtual void draw(COORD coord) const = 0;
    virtual ~IShape() = default;
};

class Reactangle : public IShape
{
public:
    Reactangle() :
        _high(0.0),
        _width(0.0)
    {

    }

    Reactangle(double high, double width) :
        _high(high),
        _width(width)
    {
        if (_high < 0.0 || _width < 0.0) {
            throw std::invalid_argument("invalid argument: high and width must be greater than zero");
        }
    }

    void draw(COORD coord) const override;
    void resize(double high, double width);
    void setHigh(double high);
    void setWidth(double width);

private:
    double _high {};
    double _width {};
};

class Square : public Reactangle
{
public:
    Square(double size) :
        Reactangle(size, size)
    {
        if (size < 0.0) {
            throw std::invalid_argument("invalid argument: size must be greater than zero");
        }
    }

    void resize(double size);
};

class Circle : public IShape
{
public:
    Circle() :
        _radius(0.0)
    {

    }

    Circle(double radius) :
        _radius(radius)
    {
        if (_radius < 0.0) {
            throw std::invalid_argument("invalid argument: size must be greater than zero");
        }
    }

    void draw(COORD center) const override;

private:
    double _radius {};
};
double toRadian(double degree);
void print(const std::vector<std::shared_ptr<IShape>> &shapes, COORD begin, int spacing);

} // end shp

#endif // SHAPE_H
