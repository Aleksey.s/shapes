#include <windows.h>
#include <memory>
#include "shape.h"

int main()
{
    try {
        std::vector<std::shared_ptr<shp::IShape>> shapes;
        shapes.push_back(std::shared_ptr<shp::IShape>(new shp::Reactangle(5, 10)));
        shapes.push_back(std::shared_ptr<shp::IShape>(new shp::Square(3)));
        shapes.push_back(std::shared_ptr<shp::IShape>(new shp::Circle(4)));
        shp::print(shapes, {10, 0}, 10);
    } catch (const std::exception& ex) {
        std::clog << ex.what();
    }
    return 0;
}

