#include "shape.h"

namespace shp {

void shp::Reactangle::draw(COORD coord) const
{
    auto out = GetStdHandle(STD_OUTPUT_HANDLE);
    for(double i = 0; i < _high; ++i) {
        SetConsoleCursorPosition(out, coord);
        for(double i = 0; i < _width; ++i) {
            std::cout << char(46);
        }
        ++coord.Y;
    }
}

void shp::Reactangle::setHigh(double high)
{
    _high = high;
}

void shp::Reactangle::setWidth(double width) {
    _width = width;
}

void shp::Reactangle::resize(double high, double width)
{
    _high = high;
    _width = width;
}

void shp::Square::resize(double size)
{
    Reactangle::resize(size, size);
}

void shp::Circle::draw(COORD center) const
{
    auto out = GetStdHandle(STD_OUTPUT_HANDLE);
    for (double i = 0; i < 360; ++i) {
        COORD point;
        point.X = static_cast<int16_t>((_radius * std::cos(toRadian(i)) + center.X));
        point.Y = static_cast<int16_t>((_radius * std::sin(toRadian(i)) + center.Y));
        SetConsoleCursorPosition(out, point);
        std::cout << char(46);
    }
}

void print(const std::vector<std::shared_ptr<IShape>>& shapes, COORD begin, int spacing)
{
    for (const auto& shp: shapes) {
        if (!shp) {
            continue;
        }
        shp->draw(begin);
        begin.Y += spacing;
    }
}

double toRadian(double degree)
{
    return  (degree * M_PI) / 180;
}

} // end shp
